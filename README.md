Vodafone  Tech Test
=======================
Author: Horia Musat <horiam@gmail.com>

"Ten Pin Bowling Score"

Prerequisite:

* Java 8 jdk

* Apache Maven

To build the app:

* mvn clean install

To run it

* java -jar ./target/vodafone-test-1.0-SNAPSHOT.jar 1 2 3 4

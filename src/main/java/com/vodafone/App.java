package com.vodafone;


public class App {

    public static void main(String[] args) throws Exception {

        int score = processScore(args);
        System.out.println(score);
    }

    public static int processScore(String... shots) throws Exception {
        return processFrame(0, 0, 0, shots);
    }

    private static int processFrame(int lastScore, int shotIndex, int frameCount, String[] shots) throws Exception {

        if (shotIndex >= shots.length || frameCount >= 10) {
            return lastScore;
        }

        int firstShot  = parseShot(shots, shotIndex);
        int secondShot = parseShot(shots, shotIndex+1);
        int thirdShot  = parseShot(shots, shotIndex+2);

        int nextFrameIndex;
        int frameScore;

        if (firstShot == 10) { // strike
            frameScore = firstShot + secondShot + thirdShot;
            nextFrameIndex = shotIndex + 1;
        } else {
            frameScore = firstShot + secondShot;

            if (frameScore == 10) { // spare
                frameScore = frameScore + thirdShot;
            }
            nextFrameIndex = shotIndex + 2;
        }

        int currentScore = lastScore + frameScore;

        return processFrame(currentScore, nextFrameIndex, ++frameCount, shots);
    }

    private static int parseShot(String[] args, int index) throws Exception {

        if (index < args.length) {
            int parsedScore = Integer.parseInt(args[index]);

            if (parsedScore >= 0 && parsedScore <= 10) {
                return parsedScore;
            } else {
                throw new Exception(parsedScore + " is an incorrect score for a ball roll");
            }
        }

        return 0;
    }
}

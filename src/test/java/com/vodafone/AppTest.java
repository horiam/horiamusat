package com.vodafone;

import java.util.Arrays;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class AppTest {

    private String[] shots;

    @Before
    public void setup() {
        shots = new String[22];
        Arrays.fill(shots, "0");
    }

    @Test
    public void shouldReturnScoreForBasicShots() throws Exception {
        int returnedScore = App.processScore("1", "2", "3", "4");
        assertEquals("Should return correct score for basic shots", 10, returnedScore);
    }

    @Test
    public void shouldReturnScoreForSpareFrame() throws Exception {
        int returnedScore = App.processScore("9", "1", "9", "1");
        assertEquals("Should return correct score for spare frame", 29, returnedScore);
    }

    @Test
    public void shouldReturnScoreForStrikeFrame() throws Exception {
        int returnedScore = App.processScore("1", "1", "1", "1", "10", "1", "1");
        assertEquals("Should return correct score for strike frame", 18, returnedScore);
    }

    @Test
    public void shouldReturnScoreForPerfectGame() throws Exception {
        String[] strikeShots = new String[13];
        Arrays.fill(strikeShots, "10");
        strikeShots[12] = "10"; // this should be ignored

        int returnedScore = App.processScore(strikeShots);
        assertEquals("Should correct return score for perfect game", 300, returnedScore);
    }

    @Test
    public void shouldIgnoreAllShotsAfterTheLastShot() throws Exception {
        shots[18] = "0"; // first shot in 10th frame
        shots[19] = "1"; // last shot of the game
        shots[20] = "1"; // this should be ignored
        shots[21] = "1"; // this should be ignored

        int returnedScore = App.processScore(shots);
        assertEquals("Should ignore extra shots",  1, returnedScore);
    }

    @Test
    public void shouldCountBonusShotForSpareLastInFrame() throws Exception {
        shots[18] = "9"; // first shot in 10th frame
        shots[19] = "1"; // that makes a spare
        shots[20] = "1"; // the bonus shot
        shots[21] = "1"; // this should be ignored

        int returnedScore = App.processScore(shots);
        assertEquals("Should count bonus shot for spare in last frame ", 11, returnedScore);
    }

    @Test
    public void shouldReturnScoreForStrikeInLastFrame() throws Exception {
        shots[18] = "10"; // strike in last frame
        shots[19] =  "1"; // first bonus shot
        shots[20] =  "1"; // second bonus shot
        shots[21] =  "1"; // this should be ignored

        int returnedScore = App.processScore(shots);
        assertEquals("Should count bonus shots for strike in last frame", 12, returnedScore);
    }

    @Test
    public void shouldCountMissingShotsAsZeroWhenLastShotIsStrike() throws Exception {
        int returnedScore = App.processScore("10");
        assertEquals("Should count missing shots as zero when last shot is strike", 10, returnedScore);
    }

    @Test
    public void shouldCountMissingShotsAsZeroWhenLastShotIsSpare() throws Exception {
        int returnedScore = App.processScore("9", "1");
        assertEquals("Should count missing shots as zero when last shot is spare", 10, returnedScore);
    }

    @Test
    public void shouldThrowExceptionForWrongShotInput() {
        try {
            App.processScore("11");
            fail();
        } catch (Exception expected) {
            assertEquals("Should fail with message", expected.getMessage(), "11 is an incorrect score for a ball roll");
        }
    }
}
